<?php

// override core en language system validation or define your own en language validation message
return [
    'emailRequired' => 'Please enter your email',
    'passwordRequired' => 'Please enter your password',
    'usernameRequired' => 'Please enter username',    
    'passwordConfirmRequired' => 'Please confirm your password'
];
