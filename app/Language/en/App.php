<?php

return [
    
    'logout' => 'Logout',
    'loginTitle' => 'Login to your account',

    // Dashboard
    'dashboard' => 'Dashboard',

    // User
    'users' => 'Users',
    'userManagementTable' => 'All Users',
    'userManagementTitle' => 'User Management',
    'userManagementSubTitle' => 'Carefuly manage user here',
    'userAdd' => 'Add User',

];