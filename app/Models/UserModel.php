<?php

declare(strict_types=1);

namespace App\Models;

use CodeIgniter\Shield\Models\UserModel as ShieldUserModel;
use CodeIgniter\HTTP\IncomingRequest;

class UserModel extends ShieldUserModel
{
    protected $table = 'users';
    protected $input;

    public function __construct(IncomingRequest $request)
    {
        parent::__construct();
        $this->input = $request;
    }

    protected function initialize(): void
    {
        
    }

    public function getUsers(): array
    {        
        $data = [];
        
        $query = $this->table($this->table)
                        ->join('auth_identities', 'users.id = auth_identities.user_id', 'left')
                        ->get();

        if($query->getNumRows() > 0)
        {
            foreach($query->getResult() as $row)
            {
                $data[] = $row;
            }           
        }

        $query->freeResult();
        return $data;        
    }

    public function getUserData(bool $count = false)
    {
        $data = [];

        $page = $this->input->getGet('page')-1;
		$per_page = $this->input->getGet('per_page');
		$sort_init = $this->input->getGet('sort');
		$sort = str_replace('|',' ',$sort_init);
		$filter = $this->input->getGet('filter') ?? '';	
        
        $offset = $per_page * $page;

        $this->table($this->table)
            ->join('auth_identities', 'users.id = auth_identities.user_id', 'left')
            ->orderBy($sort)
                ->groupStart()
                    ->like('username', $filter)
                    ->orLike('secret', $filter)
                ->groupEnd();

        if($count === true)
        {
            return $this->countAllResults();
        }
        else
        {
            $this->limit((int)$per_page, (int)$offset);
            $query = $this->get();

            if($query->getNumRows() > 0)
            {
                foreach($query->getResult() as $row)
                {
                    $data[] = $row;
                }           
            }
    
            $query->freeResult();
            return $data;                    
        }
        
    }
}
