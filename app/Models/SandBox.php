<?php

namespace App\Models;

use CodeIgniter\Model;

class SandBox extends Model
{    
    protected $table = 'users';   

    public function getUsers()
    {
        $data = [];
        $query = $this->table('users')->get();

        if($query->getNumRows() > 1)
        {
            foreach($query->getResult() as $row)
            {
                $data[] = $row;
            }
        }

        $query->freeResult();
        return $data;
    }
}
