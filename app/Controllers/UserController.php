<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;
use CodeIgniter\Shield\Entities\User;
use CodeIgniter\API\ResponseTrait;

class UserController extends BaseController
{
    use ResponseTrait;
    protected $request;
    
    public function __construct()
    {        
        if (!auth()->loggedIn()) {
            return redirect()->route('login');
        }
        $this->request = \Config\Services::request();
    }

    public function index()
    {        
        if (!auth()->loggedIn()) {
            return redirect()->route('login');
        } 

        $userModel = new UserModel($this->request);
        
        $data['title'] = lang('App.userManagementTitle');
        $data['users'] = $userModel->getUsers();

        return view('default/users', $data);
    }

    public function addUser()
    {
        if (!auth()->loggedIn()) {
            return redirect()->route('login');
        } 

        $data['title'] = lang('App.userAdd');

        return view('default/user_add', $data);
    }

    public function store()
    {
        // $users = new UserModel();
        $users = auth()->getProvider();

        $user = new User([
            'username' => 'nganuh',
            'email'    => 'nganuh@example.com',
            'password' => 'password',
        ]);

        $users->save($user);
        $user = $users->findById($users->getInsertID());

        $users->addToDefaultGroup($user);
    }

    public function editUser($id)
    {
        if (!auth()->loggedIn()) {
            return redirect()->route('login');
        } 

        $users = new UserModel($this->request);
        $data['user'] = $users->findById($id);
        $data['title'] = 'Edit '.$users->findById($id)->username;

        return view('default/user_edit', $data);
    }

    public function updateUser($id)
    {

    }

    public function userData()
    {
        $users = new UserModel($this->request);
        
        $data['total'] = $users->getUserData(true);
        $data['per_page'] = $this->request->getGet('per_page');
        $data['current_page'] = $this->request->getGet('page');
        $data['last_page'] = ceil($data['total']/$data['per_page']);
        $data['from'] = ($data['current_page'] * $data['per_page']) - ($data['per_page'] - 1);
        $data['to'] = $data['per_page'] * $data['current_page'];
        $data['data'] = $users->getUserData();

        return $this->respond($data);
        
    }
}
