<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class DashboardController extends BaseController
{    
    public function index()
    {
        if (!auth()->loggedIn()) {
            return redirect()->route('login');
        }  

        $data['title'] = lang('App.dashboard');
        
        return view('default/dashboard', $data);
    }
}
