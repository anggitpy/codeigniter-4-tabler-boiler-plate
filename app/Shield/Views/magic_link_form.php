<?= $this->extend(config('Auth')->views['layout']) ?>

<?= $this->section('title') ?><?= lang('Auth.useMagicLink') ?> <?= $this->endSection() ?>

<?= $this->section('main') ?>

<div class="card card-md">
    <div class="card-body">
        <h2 class="h2 text-center mb-4"><?= lang('Auth.useMagicLink') ?></h2>

            <?php if (session('error') !== null) : ?>
                <div class="alert alert-danger" role="alert"><?= session('error') ?></div>
            <?php elseif (session('errors') !== null) : ?>
                <div class="alert alert-danger" role="alert">
                    <?php if (is_array(session('errors'))) : ?>
                        <?php foreach (session('errors') as $error) : ?>
                            <?= $error ?>
                            <br>
                        <?php endforeach ?>
                    <?php else : ?>
                        <?= session('errors') ?>
                    <?php endif ?>
                </div>
            <?php endif ?>

        <form action="<?= url_to('magic-link') ?>" method="post">
            <?= csrf_field() ?>

            <!-- Email -->
            <div class="mb-2">
                <input type="email" class="form-control" name="email" autocomplete="email" placeholder="<?= lang('Auth.email') ?>"
                        value="<?= old('email', auth()->user()->email ?? null) ?>" required />
            </div>

            <div class="d-grid col-12 mx-auto m-3">
                <button type="submit" class="btn btn-primary w-100"><?= lang('Auth.send') ?></button>
            </div>

        </form>
    </div>
</div>

<div class="text-center text-muted mt-3">
	<?php if (setting('Auth.allowRegistration')) : ?>
		<?= lang('Auth.needAccount') ?> <a href="<?= url_to('register') ?>"><?= lang('Auth.register') ?></a>
	<?php endif ?>
</div>

<div class="text-center text-muted mt-1">
	<?php if (setting('Auth.allowMagicLinkLogins')) : ?>
		<?= lang('Auth.forgotPassword') ?> <a href="<?= url_to('magic-link') ?>"><?= lang('Auth.useMagicLink') ?></a>
	<?php endif ?>
</div>

<?= $this->endSection() ?>
