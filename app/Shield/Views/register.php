<?= $this->extend(config('Auth')->views['layout']) ?>

<?= $this->section('title') ?><?= lang('Auth.register') ?> <?= $this->endSection() ?>

<?= $this->section('main') ?>

<form class="card card-md" action="<?= url_to('register') ?>" method="post">
    <div class="card-body">
        <h2 class="card-title text-center mb-4">Create new account</h2>
        <div class="mb-3">
            <label class="form-label"><?= lang('Auth.username') ?></label>            
            <input type="text" class="form-control <?= isset(session('errors')['username']) ? 'is-invalid' : '' ?>" name="username" inputmode="text" autocomplete="username" placeholder="<?= lang('Auth.username') ?>" value="<?= old('username') ?>"  />            
            <div class="invalid-feedback"><?= isset(session('errors')['username']) ? lang('Validation.usernameRequired') : '' ?></div>
        </div>

        <div class="mb-3">
            <label class="form-label"><?= lang('Auth.email') ?></label>
            <input type="email" class="form-control <?= isset(session('errors')['email']) ? 'is-invalid' : '' ?>" name="email" inputmode="email" autocomplete="email" placeholder="<?= lang('Auth.email') ?>" value="<?= old('email') ?>"  />
            <div class="invalid-feedback"><?= isset(session('errors')['email']) ? lang('Validation.emailRequired') : '' ?></div>
        </div>
        <div class="mb-1">
            <label class="form-label"><?= lang('Auth.password') ?></label>
            <input type="password" class="form-control <?= isset(session('errors')['password']) ? 'is-invalid' : '' ?>" name="password" inputmode="text" autocomplete="new-password" placeholder="<?= lang('Auth.password') ?>"  />
            <div class="invalid-feedback"><?= isset(session('errors')['password']) ? lang('Validation.passwordRequired') : '' ?></div>
        </div>
        <div class="mb-3">
            <input type="password" class="form-control <?= isset(session('errors')['password_confirm']) ? 'is-invalid' : '' ?>" name="password_confirm" inputmode="text" autocomplete="new-password" placeholder="<?= lang('Auth.passwordConfirm') ?>"  />
            <div class="invalid-feedback"><?= isset(session('errors')['password_confirm']) ? lang('Validation.passwordConfirmRequired') : '' ?></div>
        </div>
        <!-- <div class="mb-3">
            <label class="form-check">
                <input type="checkbox" class="form-check-input"/>
                <span class="form-check-label">Agree the <a href="./terms-of-service.html" tabindex="-1">terms and policy</a>.</span>
            </label>
        </div> -->
        <div class="form-footer">
            <button type="submit" class="btn btn-primary w-100"><?= lang('Auth.register') ?></button>
        </div>
    </div>
</form>
<div class="text-center text-muted mt-3">
    <?= lang('Auth.haveAccount') ?> <a href="<?= site_url('login') ?>" tabindex="-1"><?= lang('Auth.login') ?></a>
</div>

<?=$this->endSection() ?>