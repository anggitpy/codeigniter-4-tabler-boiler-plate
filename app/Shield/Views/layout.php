<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge" />
	<title>
        <?= config('Extra')->siteTitle ?> &mdash; <?= $this->renderSection('title') ?>
    </title>
	<!-- CSS files -->
	<!-- <link href="<?= base_url('tabler/') ?>css/tabler.min.css?1669759017" rel="stylesheet" /> -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@tabler/core@1.0.0-beta17/dist/css/tabler.min.css">
	<link href="<?= base_url('tabler/') ?>css/tabler-flags.min.css?1669759017" rel="stylesheet" />
	<link href="<?= base_url('tabler/') ?>css/tabler-payments.min.css?1669759017" rel="stylesheet" />
	<link href="<?= base_url('tabler/') ?>css/tabler-vendors.min.css?1669759017" rel="stylesheet" />
	<link href="<?= base_url('tabler/') ?>css/app.min.css?1669759017" rel="stylesheet" />
	<style>
		@import url('https://rsms.me/inter/inter.css');

		:root {
			--tblr-font-sans-serif: 'Inter Var', -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;
		}

		body {
			font-feature-settings: "cv03", "cv04", "cv11";
		}
	</style>


    <?= $this->renderSection('pageStyles') ?>

</head>

<body class="border-top-wide border-primary d-flex flex-column">
	<script src="<?= base_url('tabler/') ?>js/app-theme.min.js?1669759017"></script>
	<div class="page page-center">
		<div class="container container-tight py-4">
			<div class="text-center mb-4">
				<a href="<?= site_url() ?>" class="navbar-brand navbar-brand-autodark"><img src="<?= base_url('tabler/') ?>static/logo.svg" height="36" alt=""></a>				
				<!-- <a href="<?= site_url() ?>" class="navbar-brand navbar-brand-autodark"><img src="<?= base_url('brand/') ?>esteh.png" height="120" alt=""></a>				 -->
			</div>

			<?= $this->renderSection('main') ?>
			
		</div>
	</div>
	<!-- Libs JS -->
	<!-- Tabler Core -->
	<!-- <script src="<?= base_url('tabler/') ?>js/tabler.min.js?1669759017" defer></script> -->
	<script src="https://cdn.jsdelivr.net/npm/@tabler/core@1.0.0-beta17/dist/js/tabler.min.js"></script>
	<script src="<?= base_url('tabler/') ?>js/app.min.js?1669759017" defer></script>

    <?= $this->renderSection('pageScripts') ?>

	<script>

		var body = document.body;

		body.classList.add("theme-light");

	</script>

</body>

</html>