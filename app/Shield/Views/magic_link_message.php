<?= $this->extend(config('Auth')->views['layout']) ?>

<?= $this->section('title') ?><?= lang('Auth.useMagicLink') ?> <?= $this->endSection() ?>

<?= $this->section('main') ?>

<div class="card card-md">
    <div class="card-body">
        <h2 class="h2 text-center mb-4"><?= lang('Auth.useMagicLink') ?></h2>		

        <p><b><?= lang('Auth.checkYourEmail') ?></b></p>

        <p><?= lang('Auth.magicLinkDetails', [setting('Auth.magicLinkLifetime') / 60]) ?></p>
    </div>
</div>

<?= $this->endSection() ?>
