<?= $this->extend(config('Auth')->views['layout']) ?>

<?= $this->section('title') ?><?= lang('Auth.login') ?> <?= $this->endSection() ?>

<?= $this->section('main') ?>

<div class="card card-md">
	<div class="card-body">

		<h2 class="h2 text-center mb-4"><?= lang('App.loginTitle') ?></h2>		

		<?php if (session('message') !== null) : ?>
			<div class="alert alert-success" role="alert"><?= session('message') ?></div>
		<?php endif ?>	
		
		<form action="<?= url_to('login') ?>" method="post">
			<?= csrf_field() ?>
			
			<div class="mb-3">
				<label class="form-label"><?= lang('Auth.email') ?></label>
				<input type="email" class="form-control <?= isset(session('errors')['email']) ? 'is-invalid' : '' ?>" name="email" inputmode="email" autocomplete="email" placeholder="<?= lang('Auth.email') ?>" value="<?= old('email') ?>" />
				<div class="invalid-feedback"><?= isset(session('errors')['email']) ? lang('Validation.emailRequired') : '' ?></div>
			</div>

			<div class="mb-2">
				<label class="form-label">
					<?= lang('Auth.password') ?>
					<span class="form-label-description">
						<a href="<?= url_to('magic-link') ?>"><?= lang('Auth.forgotPassword') ?></a>
					</span>
				</label>
				
				<input type="password" class="form-control <?= isset(session('errors')['password']) ? 'is-invalid' : '' ?>" name="password" inputmode="text" autocomplete="current-password" placeholder="<?= lang('Auth.password') ?>">		
				<div class="invalid-feedback"><?= isset(session('errors')['password']) ? lang('Validation.passwordRequired') : '' ?></div>				
			
			</div>
			
			<div class="mb-2">
				<label class="form-check">
					<input type="checkbox" name="remember" class="form-check-input" <?php if (old('remember')): ?> checked<?php endif ?>>
					<span class="form-check-label"><?= lang('Auth.rememberMe') ?></span>
				</label>
			</div>
			
			<div class="form-footer">
				<button type="submit" class="btn btn-primary w-100"><?= lang('Auth.login') ?></button>
			</div>

		</form>

	</div>
	
</div>

<div class="text-center text-muted mt-3">
	<?php if (setting('Auth.allowRegistration')) : ?>
		<?= lang('Auth.needAccount') ?> <a href="<?= url_to('register') ?>"><?= lang('Auth.register') ?></a>
	<?php endif ?>
</div>

<div class="text-center text-muted mt-1">
	<?php if (setting('Auth.allowMagicLinkLogins')) : ?>
		<?= lang('Auth.forgotPassword') ?> <a href="<?= url_to('magic-link') ?>"><?= lang('Auth.useMagicLink') ?></a>
	<?php endif ?>
</div>

<?= $this->endSection() ?>