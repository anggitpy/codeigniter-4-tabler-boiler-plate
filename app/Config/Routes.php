<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

$routes->get('/', 'DashboardController::index');
$routes->get('/dashboard', 'DashboardController::index');

$routes->get('/users', 'UserController::index');
$routes->get('/users/add', 'UserController::addUser');
$routes->get('/users/save', 'UserController::store');
$routes->get('/users/data', 'UserController::userData');
$routes->get('/users/(:num)', 'UserController::editUser/$1');
$routes->post('/users/(:num)', 'UserController::updateUser/$1');
$routes->get('/user_test', 'UserController::updateUser/$1');

$routes->get('/test', 'Home::index');

service('auth')->routes($routes);
