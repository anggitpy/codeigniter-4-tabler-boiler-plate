<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;

class Extra extends BaseConfig
{
    public string $siteTitle = 'My Boiler Plate';
}
