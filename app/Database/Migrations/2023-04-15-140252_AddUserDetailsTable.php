<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddUserDetailsTable extends Migration
{
    public function up()
    {
        $fields = [
            'user_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
            ],
            'first_name' => [
                'type' => 'varchar',
                'constraint' => 100,
                'null' => true
            ],
            'last_name' => [
                'type' => 'varchar',
                'constraint' => 100,
                'null' => true
            ],
            'short_bio' => [
                'type' => 'TEXT',
                'null' => true,
            ],
        ];
    }

    public function down()
    {
        //
    }
}
