<?= $this->extend('default/layout') ?>

<?= $this->section('title') ?>
	Some Title
<?= $this->endSection() ?>

<?= $this->section('subtitle') ?>
	Some Subtitle
<?= $this->endSection() ?>

<?= $this->section('toprightbutton') ?>
<div class="btn-list">
	<span class="d-none d-sm-inline">
		<a href="#" class="btn">
			New view
		</a>
	</span>
	<a href="#" class="btn btn-primary d-none d-sm-inline-block" data-bs-toggle="modal" data-bs-target="#modal-report">
		<i class="icon ti ti-moon"></i>
		Create new report
	</a>
</div>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container-xl">
	<div class="row row-deck row-cards">
		
	</div>
</div>
<?= $this->endSection() ?>