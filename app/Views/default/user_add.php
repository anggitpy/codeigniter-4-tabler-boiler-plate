<?= $this->extend('default/layout') ?>

<?= $this->section('title') ?>
	Some Title
<?= $this->endSection() ?>

<?= $this->section('subtitle') ?>
	Some Subtitle
<?= $this->endSection() ?>

<?= $this->section('toprightbutton') ?>
<div class="btn-list">
	<span class="d-none d-sm-inline">
		<a href="#" class="btn">
			New view
		</a>
	</span>
	<a href="#" class="btn btn-primary d-none d-sm-inline-block" data-bs-toggle="modal" data-bs-target="#modal-report">
		<!-- Download SVG icon from http://tabler-icons.io/i/plus -->
		<svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
			<path stroke="none" d="M0 0h24v24H0z" fill="none" />
			<line x1="12" y1="5" x2="12" y2="19" />
			<line x1="5" y1="12" x2="19" y2="12" />
		</svg>
		Create new report
	</a>
</div>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container-xl">
	
</div>
<?= $this->endSection() ?>