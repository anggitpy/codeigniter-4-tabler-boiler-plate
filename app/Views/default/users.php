<?= $this->extend('default/layout') ?>

<?= $this->section('title') ?>
<?= lang('App.userManagementTitle') ?>
<?= $this->endSection() ?>

<?= $this->section('css') ?>
<?= $this->include('default/includes/vuetable') ?>
<?= $this->endSection() ?>

<?= $this->section('subtitle') ?>
<?= lang('App.userManagementSubTitle') ?>
<?= $this->endSection() ?>

<?= $this->section('toprightbutton') ?>

<div class="btn-list">
	<a href="<?= site_url('users/add') ?>" class="btn btn-primary d-none d-sm-inline-block">
		<i class="icon ti ti-user-plus"></i>
		Create new User
	</a>
</div>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container-xl" id="app">
	<div class="col-lg-12">

		<div class="input-icon mb-3">			
			<input type="text" class="form-control" v-model="searchFor" @keyup="setFilter" placeholder="Search Data...">
			<span class="input-icon-addon">
				<i class="icon ti ti-search"></i>				
			</span>
		</div>

		<div class="card">

			<div class="table-responsive">

				<div :class="[{'data-table': true}, loading]">
					<vuetable ref="vuetable" api-url="<?= site_url('users/data/') ?>" :fields="columns" pagination-path="" :sort-order="sortOrder" :per-page="perPage" :append-params="moreParams" detail-row-component="my-detail-row" @vuetable:cell-clicked="onCellClicked" @vuetable:load-success="onLoadSuccess" detail-row-transition="fade" :css="css.table" :row-class="onRowClass" track-by="id" @vuetable:pagination-data="onPaginationData" @vuetable:loading="showLoader" @vuetable:loaded="hideLoader">

						<template slot="actions" slot-scope="props">
							<button class="btn" @click="edit(props.rowData)">Edit</button>
							<button class="btn" @click="activate(props.rowData)">Toggle</button>
						</template>

					</vuetable>
				</div>

			</div>

		</div>

		<div class="data-table-pagination text-center">
			<vuetable-pagination-info ref="paginationInfo" :info-template="paginationInfoTemplate">
			</vuetable-pagination-info>
			<vuetable-pagination ref="pagination" @vuetable-pagination:change-page="onChangePage" :css="css.pagination">
			</vuetable-pagination>
		</div>

	</div>
</div>

<?= $this->endSection() ?>


<?= $this->section('js') ?>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vuetable-2@next"></script>

<script type="text/x-template" id="expandtemplate">

	<div class="row row-xs">

		<div class="col-lg-3">
			<div class="card">

				<div class="card-body">
					<span class="badge badge-danger mr-2">
						Test
					</span>
				</div>
			</div>
		</div>
	</div>

</script>

<script>
	Vue.component('my-detail-row', {
		template: '#expandtemplate',
		props: {
			rowData: {
				type: Object,
				required: true
			}
		},
		methods: {
			onClick(event) {
				console.log('my-detail-row: on-click', event.target)
			},
		},
		filters: {
			formatNumber(value) {
				if (value == null) return ''
				return $.number(value, 0)
			},
			formatFloat(value) {
				if (value == null) return ''
				return $.number(value, 2)
			},
			formatDate(value, fmt) {
				if (value == null) {
					return ''
				} else {
					fmt = (typeof fmt == 'undefined') ? 'D MMM YYYY' : fmt
					return moment(value, 'YYYY-MM-DD').format(fmt)
				}
			},
			enumval(value) {
				return value == '0' ?
					'<span class="badge badge-danger">No</span>' :
					'<span class="badge badge-primary">Yes</span>'
			},
		}
	})

	// <i class="icon ti ti-brand-tabler"></i>

	Vue.use(Vuetable);
	var vm = new Vue({
		el: '#app',
		data: {
			loading: '',
			searchFor: '',
			columns: [{
					name: 'id',
					title: '<i class="icon ti ti-menu"></i>',
					formatter: (value, vuetable) => {
						let icon = vuetable.isVisibleDetailRow(value) ? "down" : "right";
						return [
							'<a class="show-detail-row">',
							'<i class="icon ti ti-chevron-' + icon + '"></i>',
							"</a>"
						].join('');
					}
				},
				{
					name: 'username',
					title: 'Username',
					sortField: 'username'
				},

				{
					name: 'secret',
					title: 'Email',
					sortField: 'secret'
				},

				{
					name: 'status',
					title: 'Status',
					sortField: 'status'
				},

				{
					name: 'active',
					title: 'Active',
				},

				{
					name: 'last_active',
					title: 'Last Active',
				},

				{
					name: 'last_used_at',
					title: 'Last Login',
				},

				{
					name: 'actions',
					title: 'Actions',
				}
			],
			moreParams: [],
			sortOrder: [{
				field: 'users.id',
				direction: 'asc'
			}],
			css: {
				table: {
					tableClass: 'table table-xs',
					ascendingIcon: 'lnr lnr-chevron-up',
					descendingIcon: 'lnr lnr-chevron-down',
					detailRowClass: 'detail-row mt-5',
				},
				pagination: {
					wrapperClass: "btn-group",
					activeClass: "active",
					disabledClass: "disabled",
					pageClass: "btn btn-ghost-primary btn-square",
					linkClass: "btn btn-ghost-primary btn-square",
					icons: {
						first: "lnr lnr-arrow-left",
						prev: "lnr lnr-chevron-left",
						next: "lnr lnr-chevron-right",
						last: "lnr lnr-arrow-right"
					}
				}
			},
			//paginationComponent: 'vuetable-pagination',
			perPage: 2,
			paginationInfoTemplate: 'Page {from} to {to} from {total} item(s)',

		},

		methods: {

			setFilter() {
				this.moreParams = {
					'filter': this.searchFor
				}
				this.$nextTick(function() {
					this.$refs.vuetable.refresh()
				})
			},

			formatDate(value, fmt) {
				if (value == null) return ''
				fmt = (typeof fmt == 'undefined') ? 'D MMM YYYY' : fmt
				return moment(value, 'YYYY-MM-DD').format(fmt)
			},
			formatNumber(value, fmt) {
				if (value == null) return ''
				return $.number(value, fmt)
			},

			resetFilter() {
				this.searchFor = ''
				this.setFilter()
				this.sortOrder[0].field = 'id'
				this.sortOrder[0].direction = 'asc'
			},

			showLoader() {
				this.loading = 'loading'
			},
			hideLoader() {
				this.loading = ''
			},

			edit(rowData) {

			},

			activate(rowData) {

			},

			onPaginationData(tablePagination) {
				this.$refs.paginationInfo.setPaginationData(tablePagination)
				this.$refs.pagination.setPaginationData(tablePagination)
			},
			onChangePage(page) {
				console.log(page)
				this.$refs.vuetable.changePage(page)
			},

			onInitialized(fields) {
				console.log('onInitialized', fields)
				this.vuetableFields = fields
			},

			onCellClicked(data, field, event) {
				console.log('cellClicked: ', data.data.id)
				this.$refs.vuetable.toggleDetailRow(data.data.id)
			},

			onLoadSuccess(response) {
				//console.log('Loaded: ', response)
				//this.$refs.vuetable.showDetailRow()
			},

			onDataReset() {
				console.log('onDataReset')
				this.$refs.paginationInfo.resetData()
				this.$refs.pagination.resetData()
			},

			onRowClass(dataItem, index) {
				//return (dataItem.task_type_raw === 'ipp') ? 'table-info' : 'table-warning'
			}

		},

	})
</script>

<?= $this->endSection() ?>