 <!-- Sidebar -->
 <aside class="navbar navbar-vertical navbar-expand-lg navbar-dark">
 	<div class="container-fluid">
 		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#sidebar-menu" aria-controls="sidebar-menu" aria-expanded="false" aria-label="Toggle navigation">
 			<span class="navbar-toggler-icon"></span>
 		</button>
 		<h1 class="navbar-brand navbar-brand-autodark">
 			<a href=".">
 				<img src="<?= base_url('tabler/') ?>static/logo-white.svg" width="110" height="32" alt="Tabler" class="navbar-brand-image">
 			</a>
 		</h1>
 		
 		<div class="collapse navbar-collapse" id="sidebar-menu">

 			<ul class="navbar-nav pt-lg-3">

 				<li class="nav-item">
 					<a class="nav-link" href="<?= site_url('dashboard') ?>">
					 	<span class="nav-link-icon d-md-none d-lg-inline-block">
					 		<i class="icon ti ti-brand-tabler"></i>
						</span>
 						<span class="nav-link-title">
 							<?= lang('App.dashboard') ?>
 						</span>
 					</a>
 				</li> 			 				 				

				 <li class="nav-item">
 					<a class="nav-link" href="<?= site_url('users') ?>">
					 	<span class="nav-link-icon d-md-none d-lg-inline-block">
					 		<i class="icon ti ti-brand-tabler"></i>
						</span>
 						<span class="nav-link-title">
 							<?= lang('App.users') ?>
 						</span>
 					</a>
 				</li> 	
 			
 			</ul>
 		</div>
 	</div>
 </aside>